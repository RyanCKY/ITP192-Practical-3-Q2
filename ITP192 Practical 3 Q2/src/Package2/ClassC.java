package Package2;
import Package1.ClassA;

public class ClassC {
	public static void main (String[] args) {
		ClassA ca = new ClassA();
		System.out.println("a : " + ca.a);
		System.out.println("b : " + ca.getB()); //The field ClassA.d is not visible. The variable is set to protected so it can be accessed within class, package and subclass.
		System.out.println("c : " + ca.getC()); //The field ClassA.c is not visible. By default it can only be access within the class and package.
		System.out.println("d : " + ca.getD()); //The field ClassA.d is not visible. The varibale is set to private which means that it can only be accessed within the class only.
	}
}
