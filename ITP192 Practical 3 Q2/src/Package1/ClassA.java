package Package1;

public class ClassA {
	public int a;
	protected int b;
	int c;
	private int d; //The value of the field ClassA.d is not used. The variable is set to private so it is not visible to to other classes and therefore not used.
	
	public ClassA() {
		a = 1; 
		setB(2); 
		setC(3); 
		setD(4);
	}

	public int getD() {
		return d;
	}

	public void setD(int d) {
		this.d = d;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public int getC() {
		return c;
	}

	public void setC(int c) {
		this.c = c;
	}
}