package Package1;

public class ClassB {
	public static void main(String[] args){
		ClassA ca = new ClassA();
		
		System.out.println("a : " + ca.a);
		System.out.println("b : " + ca.getB());
		System.out.println("c : " + ca.getC());
		System.out.println("d : " + ca.getD()); //The field ClassA.d is not visible. Since the variable d is private, other classes are not able to access it.
	}
}
